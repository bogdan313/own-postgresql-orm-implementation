import database.condition.GroupBy;
import database.condition.Order;
import database.condition.Where;
import entities.Car;
import entities.User;
import org.junit.*;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Main {
    private static final List<User> userList = new ArrayList<>();
    private static final List<Car> carList = new ArrayList<>();
    private static final int TOTAL_USERS = 5;
    private static final int TOTAL_CARS = 15;
    private static final Random random = new Random();

    @Before
    public void prepareData() {
        if (userList.isEmpty()) {
            for (int i = 0; i < TOTAL_USERS; i++) {
                User user = new User();

                user.setName(String.format("Name #%s", i));
                user.setLastName(String.format("Last name #%s", i));
                user.setGender(i % 2 == 0 ? 'F' : 'M');
                user.setAge(random.nextInt(100));
                user.setRegistrationDate(LocalDateTime.now());

                userList.add(user);
            }
        }

        if (carList.isEmpty()) {
            for (int i = 0; i < TOTAL_CARS; i++) {
                Car car = new Car();

                car.setManufacturer(String.format("Manufacturer #%s", i));
                car.setModel(String.format("Model #%s", i));
                car.setColor(String.format("Color #%s", random.nextInt(10)));
                car.setYear(2002 + random.nextInt(16));
                car.setProductionDate(LocalDate.of(car.getYear(), random.nextInt(11) + 1,
                        1 + random.nextInt(27)));

                carList.add(car);
            }
        }
    }

    @Test
    public void test1_insertTest() {
        userList.forEach(user -> {
            try {
                System.out.println(user.generateInsertSQL());
                assert user.save() : String.format("User '%s' not saved", user.getName());
                System.out.println(user);
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });

        carList.forEach(car -> {
            try {
                System.out.println(car.generateInsertSQL());
                assert car.save() : String.format("Car '%s' not saved", car.getModel());
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void test2_updateTest() {
        carList.forEach(car -> {
            car.setOwner(random.nextBoolean() ? userList.get(random.nextInt(TOTAL_USERS - 1)) : null);
            car.setColor(String.format("New color #%s", 15 + random.nextInt(10)));

            try {
                assert car.save() : String.format("Car %s not saved", car.getId());
                System.out.println(car);
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void test3_findOneTest() {
        userList.forEach(user -> {
            try {
                User tmpUser = (User) new User()
                        .where(new Where("name", Where.OPERATOR.EQUALS, user.getName()))
                        .andWhere(new Where("lastName", Where.OPERATOR.EQUALS, user.getLastName()));

                System.out.println(tmpUser.generateSelectSQL());

                tmpUser = tmpUser.findOne();

                if (tmpUser == null) {
                    System.out.println("User is null");
                    System.out.println(user);
                }

                assert tmpUser != null && tmpUser.equals(user)
                        : String.format("Users are different: %s %s", tmpUser.getId(), user.getId());
            }
            catch (SQLException|NullPointerException e) {
                e.printStackTrace();
            }
        });

        carList.forEach(car -> {
            try {
                Car tmpCar = new Car()
                        .where(new Where("manufacturer", Where.OPERATOR.LIKE, car.getManufacturer()))
                        .andWhere(new Where("model", Where.OPERATOR.MATCH, car.getModel()))
                        .andWhere(new Where("year", Where.OPERATOR.GREATER_OR_EQUAL, car.getYear()))
                        .findOne();

                assert tmpCar != null && tmpCar.equals(car)
                        : String.format("Cars are different: %s %s", tmpCar.getId(), car.getId());
            }
            catch (SQLException|NullPointerException e) {
                e.printStackTrace();
            }
        });
    }

    @Test
    public void test4_findTest() {
        try {
            Collection<User> users = new User().find();
            Collection<Car> cars = new Car().find();

            System.out.println(new User().generateSelectSQL());
            System.out.println(new Car().generateSelectSQL());

            users.forEach(user -> {
                System.out.println(user);
                assert userList.stream().anyMatch(user::equals) : String.format("User not found: %s", user.getId());
            });

            cars.forEach(car -> {
                System.out.println(car);
                assert carList.stream().anyMatch(car::equals) : String.format("Car not found: %s", car.getId());
            });
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test5_findTest() {
        try {
            List<Long> ids = new ArrayList<>();

            userList.stream().limit(3).forEach(user -> {
                ids.add(user.getId());
            });

            Collection<User> users = new User()
                    .where(new Where("id", Where.OPERATOR.IN, ids))
                    .find();

            users.forEach(System.out::println);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test6_countTest() {
        try {
            User user = (User) new User().groupBy(new GroupBy("id"));
            int usersCount = user.count(),
                    userListSize = userList.size();

            System.out.println(user.generateCountSQL());

            assert usersCount == userListSize :
                    String.format("Count of users not equals. %s and %s", usersCount, userListSize);

            Car car = (Car) new Car().groupBy(new GroupBy("id"));
            int carsCount = car.count(),
                    carsListSize = carList.size();

            System.out.println(car.generateCountSQL());
            System.out.println(carsCount);

            assert carsCount == carsListSize :
                    String.format("Count of cars not equals. %s and %s", carsCount, carsListSize);

            Car carsWithOwnerSql = (Car) new Car()
                    .where(new Where("id", Where.OPERATOR.GREATER, 0))
                    .andWhere(new Where("owner", "id", Where.OPERATOR.GREATER, 0));

            System.out.println(carsWithOwnerSql.generateCountSQL());

            int carsWithOwner = carsWithOwnerSql.count();

            int carsWithOwnerFromList = (int) carList.stream().filter(currentCar -> currentCar.getOwner() != null).count();

            assert carsWithOwner == carsWithOwnerFromList
                    : String.format("Cars with owner are not equal: %s != %s", carsWithOwner, carsWithOwnerFromList);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void test9_clearAll() {
        try {
            Collection<User> users =  new User().find();
            users.forEach(user -> {
                try {
                    System.out.println(user.generateDeleteSQL());
                    assert user.delete() : String.format("User not deleted: %s", user.getId());
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            });

            Collection<Car> cars = new Car().find();
            cars.forEach(car -> {
                try {
                    assert car.delete() : String.format("Car not deleted: %s", car.getId());
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
            });
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
