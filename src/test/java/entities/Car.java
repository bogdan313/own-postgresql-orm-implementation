package entities;

import database.annotations.Column;
import database.annotations.Id;
import database.annotations.Relation;
import database.base.Entity;

import java.time.LocalDate;

public class Car extends Entity {
    @Id @Column
    private long id;
    @Column private String manufacturer;
    @Column private String model;
    @Column private String color;
    @Column private int year;
    @Relation(foreignKey = "user_id", type = Relation.RELATION_TYPE.BELONGS_TO)
    private User owner;
    @Column(name = "production_date")
    private LocalDate productionDate;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getManufacturer() {
        return manufacturer;
    }
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        this.year = year;
    }

    public User getOwner() {
        return owner;
    }
    public void setOwner(User owner) {
        this.owner = owner;
    }

    public LocalDate getProductionDate() {
        return productionDate;
    }
    public void setProductionDate(LocalDate productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public String toString() {
        return String.format("Id: %s, Manufacturer: %s, Model: %s, Year: %s, Production date: %s, Owner: {%s}", getId(), getManufacturer(),
                getModel(), getYear(), getProductionDate(), getOwner() != null ? getOwner() : "");
    }

    /*public boolean equals(Car car) {
        return this.getId() == car.getId() &&
                this.getManufacturer().equals(car.getManufacturer()) &&
                this.getModel().equals(car.getModel()) &&
                this.getColor().equals(car.getColor()) &&
                this.getYear() == car.getYear();
    }*/
}
