package entities;

import database.annotations.*;
import database.base.Entity;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.stream.Collectors;

public class User extends Entity {
    @Column @Id
    private long id;
    @Column private String name;
    @Column(name = "last_name") private String lastName;
    @Column private int age;
    @Column private char gender;
    @Relation(type = Relation.RELATION_TYPE.HAS, foreignKey = "user_id")
    private Collection<Car> cars;
    @Column(name = "registration_date")
    private LocalDateTime registrationDate;

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }
    public void setGender(char gender) {
        this.gender = gender;
    }

    public Collection<Car> getCars() {
        return cars;
    }
    public void setCars(Collection<Car> cars) {
        this.cars = cars;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }
    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    @BeforeSave private void beforeSave1() {
        System.out.println("Before save 1");
    }
    @BeforeSave private void beforeSave2() {
        System.out.println("Before save 2");
    }

    @AfterFind private void afterSave1() {
        System.out.println("After find 1");
    }
    @AfterFind private void afterSave2() {
        System.out.println("After find 2");
    }

    @Override
    public String toString() {
        return String.format("Id: %s, name: %s, last name: %s, age: %s, gender: %s, registration date: %s, cars: {%s}",
                getId(), getName(), getLastName(), getAge(), getGender(), getRegistrationDate(),
                cars != null
                        ? cars.stream().filter(car -> car != null).map(Car::toString).collect(Collectors.joining(", "))
                        : ""
        );
    }

    /*public boolean equals(User user) {
        return user.getId() == getId() &&
                user.getName().equals(getName()) &&
                user.getLastName().equals(getLastName()) &&
                user.getAge() == getAge() &&
                user.getGender() == getGender();
    }*/
}
