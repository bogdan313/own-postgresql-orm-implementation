package database.condition;

/**
 * Class represents to PostgreSQL LIMIT and OFFSET block
 * Used in {@link database.base.Entity} class
 */
public class Pagination {
    private final int pageNumber;
    private final int pageSize;

    public int getPageNumber() {
        return pageNumber;
    }

    public int getPageSize() {
        return pageSize;
    }

    public Pagination(int pageNumber, int pageSize) {
        this.pageNumber = pageNumber;
        this.pageSize = pageSize;
    }

    public Pagination(int pageSize) {
        this.pageNumber = 1;
        this.pageSize = pageSize;
    }

    public String getSql() {
        StringBuilder result = new StringBuilder();

        result.append(String.format("LIMIT %s", pageSize));

        if (pageNumber > 1)
            result.append(String.format(" OFFSET %s", (pageNumber - 1) * pageSize));

        return result.toString();
    }
}
