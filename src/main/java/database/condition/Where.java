package database.condition;

/**
 * Class represents to PostgreSQL WHERE block
 * Used in {@link database.base.Entity} class
 */
public class Where {
    public enum CONDITION {
        OR, AND
    }

    public enum OPERATOR {
        EQUALS("="),
        NOT_EQUAL("!="),
        GREATER(">"),
        GREATER_OR_EQUAL(">="),
        LESS("<"),
        LESS_OR_EQUAL("<="),
        LIKE("LIKE"),
        MATCH("~*"),
        NOT_LIKE("NOT LIKE"),
        IS_NULL("IS NULL"),
        IS_NOT_NULL("IS NOT NULL"),
        IN("IN"),
        NOT_IN("NOT IN"),
        IS_TRUE("IS TRUE"),
        IS_FALSE("IS FALSE");

        private final String operator;

        OPERATOR(String operator) {
            this.operator = operator;
        }

        public String getOperator() {
            return operator;
        }

        public String toString() {
            return getOperator();
        }
    }

    private final CONDITION condition;
    private final String column;
    private final OPERATOR operator;
    private final Object value;
    private final String relation;

    public CONDITION getCondition() {
        return condition;
    }

    public String getColumn() {
        return column;
    }

    public OPERATOR getOperator() {
        return operator;
    }

    public Object getValue() {
        return value;
    }

    public String getRelation() {
        return relation;
    }

    public Where(String column, OPERATOR operator, Object value) {
        this(null, null, column, operator, value);
    }

    public Where(String column, OPERATOR operator) {
        this(null, column, operator);
    }

    public Where(CONDITION condition, String column, OPERATOR operator) {
        this(condition, null, column, operator, null);
    }

    public Where(CONDITION condition, String column, OPERATOR operator, Object value) {
        this(condition, null, column, operator, value);
    }

    public Where(String relation, String column, OPERATOR operator, Object value) {
        this(null, relation, column, operator, value);
    }

    public Where(CONDITION condition, String relation, String column, OPERATOR operator, Object value) {
        this.condition = condition;
        this.relation = relation;
        this.column = column;
        this.operator = operator;
        this.value = value;
    }

    public Where(CONDITION condition, Where where) {
        this.condition = condition;
        this.column = where.getColumn();
        this.operator = where.getOperator();
        this.value = where.getValue();
        this.relation = where.getRelation();
    }
}
