package database.condition;

/**
 * Class represents to PostgreSQL ORDER block
 * Used in {@link database.base.Entity} class
 */
public class Order {
    public enum DIRECTION {
        ASC, DESC
    }

    final private DIRECTION direction;
    final private String relation;
    final private String column;

    public DIRECTION getDirection() {
        return direction;
    }

    public String getColumn() {
        return column;
    }

    public String getRelation() {
        return relation;
    }

    public Order(DIRECTION direction, String column) {
        this(direction, null, column);
    }

    public Order(String column) {
        this(DIRECTION.ASC, null, column);
    }

    public Order(String relation, String column) {
        this(DIRECTION.ASC, relation, column);
    }

    public Order(DIRECTION direction, String relation, String column) {
        this.direction = direction;
        this.relation = relation;
        this.column = column;
    }
}
