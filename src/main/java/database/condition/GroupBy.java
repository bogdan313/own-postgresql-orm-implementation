package database.condition;

public class GroupBy {
    private final String relation;
    private final String column;

    public String getRelation() {
        return relation;
    }

    public String getColumn() {
        return column;
    }

    public GroupBy(String relation, String column) {
        this.relation = relation;
        this.column = column;
    }

    public GroupBy(String column) {
        this.relation = null;
        this.column = column;
    }
}
