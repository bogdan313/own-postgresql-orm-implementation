package database.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Relation {
    enum RELATION_TYPE {
        BELONGS_TO, HAS,
    }

    RELATION_TYPE type();
    String foreignKey();
}
