package database.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Use this annotation on class to specify table name
 * Default is class simple name
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {
    String name() default "";
}
