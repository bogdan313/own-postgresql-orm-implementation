package database.base;

import database.annotations.*;
import database.condition.GroupBy;
import database.condition.Order;
import database.condition.Pagination;
import database.condition.Where;
import database.helpers.SQLHelper;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Base class for all entities, represents table row in database
 */
public class Entity {
    /**
     * List of {@link Where} conditions. Search query where block based on this list of {@link Where}
     */
    final private List<Where> whereCondition = new ArrayList<>();

    /**
     * List of {@link Order} conditions. Search query order block based on this list of {@link Order}
     */
    final private List<Order> orderCondition = new ArrayList<>();

    /**
     * List of {@link GroupBy}. Appends to Group By query block
     */
    final private List<GroupBy> groupBy = new ArrayList<>();

    /**
     * {@link Pagination} parameter used to set LIMIT and OFFSET parameters
     */
    private Pagination pagination;

    final public Entity setPagination(Pagination pagination) {
        this.pagination = pagination;
        return this;
    }

    final public Entity where(Where... conditions) {
        whereCondition.addAll(Arrays.asList(conditions));
        return this;
    }
    final public Entity andWhere(Where where) {
        whereCondition.add(new Where(Where.CONDITION.AND, where));
        return this;
    }
    final public Entity orWhere(Where where) {
        whereCondition.add(new Where(Where.CONDITION.OR, where));
        return this;
    }

    final public Entity order(Order... order) {
        orderCondition.addAll(Arrays.asList(order));
        return this;
    }
    final public Entity addOrder(Order order) {
        orderCondition.add(order);
        return this;
    }

    final public Entity groupBy(GroupBy... groups) {
        groupBy.addAll(Arrays.asList(groups));
        return this;
    }
    final public Entity addGroupBy(GroupBy groupBy) {
        this.groupBy.add(groupBy);
        return this;
    }
    final public Entity removeGroupBy(String column) {
        return removeGroupBy(null, column);
    }
    final public Entity removeGroupBy(String relation, String column) {
        if (column != null) {
            GroupBy groupBy = this.groupBy.stream()
                    .filter(
                            item -> (relation != null && relation.equals(item.getRelation()) || relation == null) &&
                                    column.equals(item.getColumn()) || relation == null
                    )
                    .findFirst().orElse(null);

            if (groupBy != null)
                this.groupBy.remove(groupBy);
        }
        return this;
    }
    private void clearGroupBy() {
        groupBy.clear();
    }


    /**
     * Use this method to fetch all rows from table which corresponds to
     * {@link #whereCondition}, {@link #orderCondition} and {@link #pagination}
     * @return Collection of found elements
     * @throws SQLException when SQL exception occurs
     */
    final public <T extends Entity> Collection<T> find() throws SQLException {
        String findSQL = generateSelectSQL();
        Collection<T> result = (Collection<T>) Connection.select(this.getClass(), findSQL);
        result.forEach(Entity::callAfterFindMethods);
        return result;
    }


    /**
     * Use this method to fetch one row from table which corresponds to
     * {@link #whereCondition}, {@link #orderCondition} and {@link #pagination}
     * @param <T> is returned object class type
     * @return new object which represent table row
     * @throws SQLException when SQL exception occurs
     */
    final public <T extends Entity> T findOne() throws SQLException {
        setPagination(new Pagination(1, 1));
        Collection<T> result = find();
        return result.iterator().hasNext() ? result.iterator().next() : null;
    }


    /**
     * Save current object to database. If primary column exists method update will be called, else method create
     * @return if object saved
     * @throws SQLException when SQL exception occurs
     */
    final public boolean save() throws SQLException {
        return isNewRecord() ? create() : update();
    }


    /**
     * Method generates SQL for current object, execute query via {@link Connection} and set primary column from
     * returned value
     * @return if object created
     * @throws SQLException when SQL exception occurs
     */
    private boolean create() throws SQLException {
        try {
            Field idField = getIdAnnotatedField();
            String sql = generateInsertSQL();
            long id = Connection.insert(sql, getColumnNameByField(idField));
            if (id > 0) {
                try {
                    setIdAnnotatedField(id);
                    return true;
                }
                catch (IllegalAccessException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            throw e;
        }
        return false;
    }


    /**
     * Method generates SQL for updating current object with current data
     * @return if object updated
     * @throws SQLException when SQL exception occurs
     */
    private boolean update() throws SQLException {
        String sql = generateUpdateSQL();
        if (sql != null) {
            try {
                Connection.executeSql(sql);
                return true;
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }


    /**
     * Use this method to delete row corresponds to current object
     * @return if object deleted
     * @throws SQLException when SQL exception occurs
     */
    final public boolean delete() throws SQLException {
        String sql = generateDeleteSQL();
        if (sql != null) {
            try {
                Connection.executeSql(sql);
                setIdAnnotatedField(0);
                return true;
            }
            catch (SQLException e) {
                e.printStackTrace();
            }
            catch (IllegalAccessException ignore) {}
        }
        return false;
    }


    /**
     * Method generates SQL query f
     * @return count of suitable objects
     * @throws SQLException when SQL exception occurs
     */
    final public int count() throws SQLException {
        String sql = generateCountSQL();
        Connection connection = new Connection();
        connection.execute(sql);
        ResultSet resultSet = connection.getResultSet();
        int result = 0;

        while (resultSet.next()) {
            result += resultSet.getInt("count");
        }

        return result;
    }


    /**
     * Check if annotation {@link Table} exists on current class and return {@link Table#name()} or class name as table name
     * @return table name for current class
     */
    private String getTableName() {
        return getTableName(this.getClass());
    }

    private static String getTableName(Class<?> clazz) {
        Table table = clazz.getDeclaredAnnotation(Table.class);
        return table != null && !table.name().isEmpty()
                ? table.name() : clazz.getSimpleName().toLowerCase();
    }


    /**
     * Generates SQL query for getting data from database
     * @return generated SQL query string
     * @throws SQLException when SQL exception occurs
     */
    public String generateSelectSQL() throws SQLException {
        StringBuilder result = new StringBuilder();

        appendSelect(result);
        appendFromWhereOrder(result);
        appendGroupBy(result);
        appendOrder(result);
        appendEnding(result);

        return result.toString();
    }


    /**
     * Check {@link Column} and {@link Relation.RELATION_TYPE#BELONGS_TO} annotated fields
     * Collect column names and object values
     * @param withIdAnnotatedField if column with {@link Id} annotation is needed
     * @return Map<String, Object> where key is column name in table, value of corresponding field
     */
    private Map<String, Object> getColumnNamesAndValues(boolean withIdAnnotatedField) {
        Map<String, Object> result = new HashMap<>();

        getColumnAnnotatedFields(withIdAnnotatedField).forEach(field -> {
            field.setAccessible(true);
            result.put(getColumnNameByField(field), SQLHelper.getFieldValue(field, this));
        });

        getBelongsToRelationFields().forEach(field -> {
            field.setAccessible(true);
            try {
                result.put(getForeignKeyByRelationField(field),
                        SQLHelper.getSQLString(getRelationFieldPrimaryKeyValue(getEndType(field), field.get(this))));
            }
            catch (IllegalAccessException e) {
                result.put(getForeignKeyByRelationField(field), SQLHelper.getSQLString(null));
            }
        });

        return result;
    }

    /**
     * Generates SQL query for insert current object to database
     * @return generated SQL query string
     * @throws SQLException when SQL exception occurs
     */
    public String generateInsertSQL() throws SQLException {
        StringBuilder result = new StringBuilder();
        Map<String, Object> fieldsAndValues = getColumnNamesAndValues(false);

        if (fieldsAndValues.isEmpty()) throw new SQLException("No fields to save");

        result.append(String.format("INSERT INTO \"%s\" (%s) VALUES(%s)",
                getTableName(),
                fieldsAndValues.entrySet().stream().map(Map.Entry::getKey).collect(Collectors.joining(", ")),
                fieldsAndValues.entrySet().stream().map(stringObjectEntry -> stringObjectEntry.getValue().toString())
                        .collect(Collectors.joining(", "))
        ));

        appendEnding(result);

        return result.toString();
    }


    /**
     * Generates SQL query for update current object in database
     * @return generated SQL query string
     * @throws SQLException when SQL exception occurs
     */
    public String generateUpdateSQL() throws SQLException {
        long idAnnotatedFieldValue = getIdAnnotatedFieldValue();

        if (idAnnotatedFieldValue == 0) return null;

        StringBuilder result = new StringBuilder();
        Map<String, Object> fieldsAndValues = getColumnNamesAndValues(false);

        if (fieldsAndValues.isEmpty()) throw new SQLException("No fields to save");

        result.append(String.format("UPDATE \"%s\" SET %s WHERE %s = %s", getTableName(),
                fieldsAndValues.entrySet().stream()
                        .map(stringObjectEntry ->
                                String.format("%s = %s", stringObjectEntry.getKey(),
                                        stringObjectEntry.getValue()))
                        .collect(Collectors.joining(", ")),
                getColumnNameByField(getIdAnnotatedField()),
                SQLHelper.getFieldValue(getIdAnnotatedField(), this)
        ));
        appendEnding(result);

        return result.toString();
    }


    /**
     * Generate SQL query for deleting current object from database
     * @return generated SQL query string
     * @throws SQLException when SQL exception occurs
     */
    public String generateDeleteSQL() throws SQLException {
        if (getIdAnnotatedFieldValue() > 0) {
            StringBuilder result = new StringBuilder();

            result.append(String.format("DELETE FROM \"%s\" ", getTableName()));
            Field idAnnotatedField = getIdAnnotatedField();

            if (idAnnotatedField == null) throw new SQLException("No primary key field");

            result.append(String.format("WHERE %s = %s", getColumnNameByField(getIdAnnotatedField()),
                    getIdAnnotatedFieldValue()));
            appendEnding(result);

            return result.toString();
        }

        return null;
    }


    /**
     * Generate SQL query for count data in table
     * @return generated SQL string
     * @throws SQLException when SQL exception occurs
     */
    public String generateCountSQL() throws SQLException {
        StringBuilder sql = new StringBuilder();
        Field idAnnotatedField = getIdAnnotatedField();
        if (idAnnotatedField != null) {
            sql.append(String.format("SELECT count(DISTINCT \"%s\".\"%s\") as count FROM \"%s\" ",
                    getTableName(),
                    getColumnNameByField(idAnnotatedField),
                    getTableName()));
            appendOwnAndBelongsToWhereConditions(sql);
            appendEnding(sql);

            return sql.toString();
        }
        else throw new SQLException("No id annotated field");
    }


    /**
     * Append SELECT block with relations to the parameter
     * @param sql existed string with SQL query
     * @throws SQLException when SQL exception occurs
     */
    private void appendSelect(StringBuilder sql) throws SQLException {
        Map<String, String> selectColumns = getSelectColumnsAndAppendToGroupBy();

        if (selectColumns.isEmpty()) throw new SQLException("No fields to find");

        sql.append(String.format("SELECT %s ", selectColumns.entrySet().stream()
                .map(entry -> String.format("%s as %s", entry.getKey(), entry.getValue()))
                .collect(Collectors.joining(", ")))
        );
    }


    /**
     * Check {@link Column} or {@link Relation} annotated fields and generate map of <column name, alternative name> pairs
     * @return Map<String, String> key is tableName_columnName (or relationName_tableName_columnName for relations)
     * value is alternative name for column
     */
    private Map<String, String> getSelectColumnsAndAppendToGroupBy() {
        Map<String, String> result = new HashMap<>();

        clearGroupBy();

        getColumnAnnotatedFields(true).forEach(field -> {
            String tableName = getTableName(),
                    columnName = getColumnNameByField(field);

            addGroupBy(new GroupBy(field.getName()));

            result.put(
                    String.format("\"%s\".\"%s\"", tableName, columnName),
                    String.format("\"%s_%s\"", tableName, columnName)
            );
        });

        List<Field> relationFields = getBelongsToRelationFields();
        relationFields.addAll(getHasRelationFields());

        relationFields.forEach(field -> {
            final String relationName = field.getName(),
                    tableName = getTableName(getEndType(field));
            List<Field> relationObjectFields = new ArrayList<>();
            getColumnAnnotatedFields(relationObjectFields, getEndType(field), true);

            relationObjectFields.forEach(relationField -> {

                addGroupBy(new GroupBy(field.getName(), relationField.getName()));

                result.put(
                        String.format("\"%s\".\"%s\"", String.format("%s_%s", relationName, tableName),
                                getColumnNameByField(relationField)),
                        String.format("\"%s_%s_%s\"", relationName, tableName, getColumnNameByField(relationField))
                );
            });
        });

        return result;
    }


    /**
     * Apped FROM block with LEFT JOIN relations, ORDER BY block, LIMIT and OFFSET blocks
     * (fields with {@link Relation} annotation)
     * @param sql existed string with SQL query
     * @throws SQLException when SQL exception occurs
     */
    private void appendFromWhereOrder(StringBuilder sql) throws SQLException {
        appendFromWhereOrder(sql, true);
    }


    /**
     * Appends FROM block for own table and LEFT JOIN relation tables if parameter withJoin is set to true
     * @param sql {@link StringBuilder} in which result will be appended
     * @param withJoin if LEFT JOIN block needed
     * @throws SQLException when SQL exception occurs
     */
    private void appendFromWhereOrder(StringBuilder sql, boolean withJoin) throws SQLException {
        String paginationString = "";

        if (pagination != null)
            paginationString = pagination.getSql();

        StringBuilder whereSql = new StringBuilder();
        appendOwnAndBelongsToWhereConditions(whereSql);

        StringBuilder orderSql = new StringBuilder();
        appendOrder(orderSql);

        sql.append(String.format("FROM (SELECT * FROM \"%s\" %s %s %s) as \"%s\" ",
                getTableName(), whereSql.toString(), orderSql.toString(), paginationString, getTableName()));

        if (withJoin) {
            appendRelationTables(sql);
        }

        sql.append(" ");
    }


    /**
     * Appends LEFT JOIN block and all tables of {@link Relation} annotated fields
     * @param sql {@link StringBuilder} in which result will be appended
     */
    private void appendRelationTables(StringBuilder sql) {
        List<Field> relationFields = getBelongsToRelationFields();
        relationFields.addAll(getHasRelationFields());

        sql.append(
                relationFields.stream().map(field -> {
                    Relation.RELATION_TYPE relationType = field.getDeclaredAnnotation(Relation.class).type();
                    String relationTableName = getTableName(getEndType(field));
                    return String.format("LEFT JOIN \"%s\" as \"%s_%s\" on \"%s_%s\".\"%s\" = \"%s\".\"%s\"",
                            relationTableName,
                            field.getName(),
                            relationTableName,
                            field.getName(),
                            relationTableName,
                            relationType.equals(Relation.RELATION_TYPE.BELONGS_TO)
                                    ? getColumnNameByField(getIdAnnotatedField(getEndType(field)))
                                    : getForeignKeyByRelationField(field),
                            getTableName(),
                            relationType.equals(Relation.RELATION_TYPE.BELONGS_TO)
                                    ? getForeignKeyByRelationField(field)
                                    : getColumnNameByField(getIdAnnotatedField(getEndType(field)))
                    );
                })
                .collect(Collectors.joining(" "))
        );
    }


    /**
     * Appends WHERE block and conditions in {@link Entity#whereCondition} attribute
     * @param sql {@link StringBuilder} in which result will be appended
     * @throws SQLException when SQL exception occurs
     */
    private void appendWhere(StringBuilder sql) throws SQLException {
        if (!whereCondition.isEmpty()) {
            sql.append("WHERE ");
            boolean appendCondition = false;

            for (Where where : whereCondition) {
                appendWhereCondition(sql, where, appendCondition);
                appendCondition = true;
            }

            sql.append(" ");
        }
    }

    private void appendOwnAndBelongsToWhereConditions(StringBuilder sql) throws SQLException {
        if (!whereCondition.isEmpty()) {
            List<Where> ownWhereCondition = whereCondition.stream()
                    .filter(where -> where.getRelation() == null || where.getRelation().trim().isEmpty())
                    .collect(Collectors.toList());

            List<Where> relationWhereCondition = whereCondition.stream()
                    .filter(where -> where.getRelation() != null)
                    .collect(Collectors.toList());

            boolean appendCondition = false;

            if (!ownWhereCondition.isEmpty() || !relationWhereCondition.isEmpty()) {
                sql.append("WHERE ");

                for (Where where : ownWhereCondition) {
                    appendWhereCondition(sql, where, appendCondition);
                    appendCondition = true;
                }

                for (Where where : relationWhereCondition) {
                    appendBelongsToRelationWhereCondition(sql, where, appendCondition);
                    appendCondition = true;
                }
            }
        }
    }

    private void appendOwnWhere(StringBuilder sql) throws SQLException {
        if (!whereCondition.isEmpty()) {
            List<Where> ownWhereCondition = whereCondition.stream()
                    .filter(where -> where.getRelation() == null || where.getRelation().trim().isEmpty())
                    .collect(Collectors.toList());

            if (!ownWhereCondition.isEmpty()) {
                sql.append("WHERE ");
                boolean appendCondition = false;

                for (Where where : ownWhereCondition) {
                    appendWhereCondition(sql, where, appendCondition);
                    appendCondition = true;
                }
            }
        }
    }

    private void appendRelationWhere(StringBuilder sql) throws SQLException {
        if (!whereCondition.isEmpty()) {
            List<Where> relationWhereCondition = whereCondition.stream()
                    .filter(where -> where.getRelation() != null)
                    .collect(Collectors.toList());

            if (!relationWhereCondition.isEmpty()) {
                sql.append("WHERE ");
                boolean appendCondition = false;

                for (Where where : relationWhereCondition) {
                    appendWhereCondition(sql, where, appendCondition);
                    appendCondition = true;
                }
            }
        }
    }

    private void appendBelongsToRelationWhere(StringBuilder sql) throws SQLException {
        if (!whereCondition.isEmpty()) {
            List<Where> relationWhereCondition = whereCondition.stream()
                    .filter(where -> where.getRelation() != null)
                    .collect(Collectors.toList());

            if (!relationWhereCondition.isEmpty()) {
                sql.append("WHERE ");
                boolean appendCondition = false;

                for (Where where : relationWhereCondition) {
                    appendBelongsToRelationWhereCondition(sql, where, appendCondition);
                    appendCondition = true;
                }
            }
        }
    }

    private void appendWhereCondition(StringBuilder sql, Where where, boolean appendCondition) throws SQLException {
        String fieldName = where.getRelation() != null && !where.getRelation().isEmpty()
                ? where.getRelation()
                : where.getColumn();

        if (hasField(fieldName)) {
            Field currentField = getFieldByName(fieldName);

            if (currentField != null) {
                String tableName;
                String columnName;

                Relation relation = currentField.getDeclaredAnnotation(Relation.class);

                if (relation != null) {
                    Class relationType = getEndType(currentField);
                    Field relationField = getFieldByName(relationType, where.getColumn());

                    if (relationField == null) {
                        throw new SQLException(String.format("Field not found: %s", where.getColumn()));
                    }

                    columnName = getColumnNameByField(relationField);
                    tableName = String.format("%s_%s", currentField.getName(), getTableName(relationType));
                }
                else {
                    tableName = getTableName();
                    columnName = getColumnNameByField(currentField);
                }

                if (appendCondition) {
                    sql.append(String.format("%s ",
                            where.getCondition() != null ? where.getCondition() : Where.CONDITION.AND));
                }

                sql.append(String.format("\"%s\".\"%s\" %s %s ",
                        tableName, columnName, where.getOperator(),
                        SQLHelper.getSQLString(where.getValue())));
            }
        }
        else {
            throw new SQLException(String.format("Field not found: %s", fieldName));
        }
    }

    private void appendBelongsToRelationWhereCondition(StringBuilder sql, Where where, boolean appendCondition) {
        Field relationField = getFieldByName(where.getRelation());

        if (relationField != null) {
            Relation relation = relationField.getDeclaredAnnotation(Relation.class);

            if (relation != null && relation.type() == Relation.RELATION_TYPE.BELONGS_TO) {
                if (appendCondition) {
                    sql.append(String.format("%s ",
                            where.getCondition() != null ? where.getCondition() : Where.CONDITION.AND));
                }

                sql.append(String.format("\"%s\".\"%s\" %s %s ",
                        getTableName(), relation.foreignKey(), where.getOperator(),
                        SQLHelper.getSQLString(where.getValue())));
            }
        }
    }

    private void appendOrder(StringBuilder sql) throws SQLException {
        if (!orderCondition.isEmpty()) {
            sql.append(String.format("ORDER BY %s ", prepareOrderConditions(orderCondition.toArray(new Order[]{}))
                    .stream()
                    .collect(Collectors.joining(", "))
            ));
        }
    }

    private List<String> prepareOrderConditions(Order... orders) throws SQLException {
        List<String> result = new ArrayList<>();

        for (Order order : orders) {
            String fieldName = order.getColumn();

            if (hasField(fieldName)) {
                Field currentField = getFieldByName(fieldName);
                String tableName = getTableName(),
                        columnName = getColumnNameByField(currentField);

                result.add(String.format("\"%s\".\"%s\" %s", tableName, columnName,
                        order.getDirection()));
            }
            else {
                throw new SQLException(String.format("Field not found: %s", fieldName));
            }
        }

        return result;
    }

    private void appendPagination(StringBuilder sql) {
        if (pagination != null)
            sql.append(String.format("%s", pagination.getSql()));
    }

    private void appendGroupBy(StringBuilder sql) throws SQLException {
        if (!groupBy.isEmpty()) {
            sql.append(String.format("GROUP BY %s ", prepareGroupByConditions(groupBy.toArray(new GroupBy[]{}))
                    .stream().collect(Collectors.joining(", "))));
        }
    }

    private List<String> prepareGroupByConditions(GroupBy... groups) throws SQLException {
        List<String> result = new ArrayList<>();

        for (GroupBy currentGroupBy : groups) {
            Field field = getFieldByName(currentGroupBy.getRelation() != null
                    ? currentGroupBy.getRelation() : currentGroupBy.getColumn());

            if (field != null) {
                Relation relation = field.getDeclaredAnnotation(Relation.class);
                String tableName,
                        columnName;

                if (relation != null) {
                    Class relationType = getEndType(field);
                    Field relationField = getFieldByName(relationType, currentGroupBy.getColumn());

                    if (relationField == null) {
                        throw new SQLException(String.format("Field not found: %s", currentGroupBy.getColumn()));
                    }

                    columnName = getColumnNameByField(relationField);
                    tableName = String.format("%s_%s", field.getName(), getTableName(relationType));
                }
                else {
                    tableName = getTableName();
                    columnName = getColumnNameByField(field);
                }

                result.add(String.format("\"%s\".\"%s\"", tableName, columnName));
            }
        }

        return result;
    }

    private void appendEnding(StringBuilder sql) {
        sql.append(";");
    }


    /**
     * Check if this is a new record or existing
     * @return if {@link Id} annotated field value greater than 0
     * @throws SQLException when {@link Id} annotated field not found
     */
    public boolean isNewRecord() throws SQLException {
        return getIdAnnotatedFieldValue() == 0;
    }


    /**
     * Get fields with {@link Column} annotation
     * @param withIdAnnotatedField if field with {@link Id} annotation required
     * @return List of appropriated fields
     */
    private List<Field> getColumnAnnotatedFields(boolean withIdAnnotatedField) {
        List<Field> fields = new ArrayList<>();
        getColumnAnnotatedFields(fields, this.getClass(), withIdAnnotatedField);

        return fields;
    }

    private static void getColumnAnnotatedFields(List<Field> fields, Class<?> clazz, boolean withIdAnnotatedField) {
        if (clazz == null) return;

        getColumnAnnotatedFields(fields, clazz.getSuperclass(), withIdAnnotatedField);

        for (Field field : clazz.getDeclaredFields()) {
            boolean add = field.isAnnotationPresent(Column.class),
                    isId = field.isAnnotationPresent(Id.class);

            if (!withIdAnnotatedField && isId) add = false;

            if (add) {
                fields.add(field);
            }
        }
    }


    /**
     * Check class for field which annotated by {@link Id} and return it
     * @return {@link Field} which has {@link Id} annotation
     */
    private Field getIdAnnotatedField() {
        List<Field> fields = getFieldsByAnnotation(Id.class);

        return fields.stream().findFirst().orElse(null);
    }

    private static Field getIdAnnotatedField(Class<?> clazz) {
        List<Field> fields = new ArrayList<>();
        getFieldsByAnnotation(fields, Id.class, clazz);

        return fields.stream().findFirst().orElse(null);
    }

    static String getIdAnnotatedFieldColumnName(Entity entity) {
        Field field = entity.getIdAnnotatedField();
        if (field == null) return null;

        return getColumnNameByField(field);
    }

    private long getIdAnnotatedFieldValue() {
        Field field = getIdAnnotatedField();
        field.setAccessible(true);

        if (field.getType().equals(Long.class) || field.getType().equals(long.class) ||
                field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
            try {
                return Long.parseLong(field.get(this).toString());
            }
            catch (IllegalAccessException e) {}
        }

        return 0;
    }

    private void setIdAnnotatedField(long value) throws IllegalAccessException {
        Field field = getIdAnnotatedField();
        if (field != null) {
            field.setAccessible(true);

            if (field.getType().equals(Long.class) || field.getType().equals(long.class)) {
                field.set(this, value);
            }
        }
    }

    private void setIdAnnotatedField(int value) throws IllegalAccessException {
        Field field = getIdAnnotatedField();
        if (field != null) {
            field.setAccessible(true);

            if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
                field.set(this, value);
            }
        }
    }


    /**
     * Find {@link Column} annotation, check if {@link Column#name()} is not empty
     * @param field which refers to column in table
     * @return {@link Column#name()} if not empty or {@link Field#getName()}
     */
    private static String getColumnNameByField(Field field) {
        field.setAccessible(true);
        Column annotation = field.getDeclaredAnnotation(Column.class);
        return annotation != null && !annotation.name().isEmpty()
                ? annotation.name() : field.getName();
    }


    /**
     * Check if field with #fieldName is exists or its parent
     * @param fieldName name of searching field
     * @return if field exists in class or superclasses
     */
    private boolean hasField(String fieldName) {
        return hasField(this.getClass(), fieldName);
    }


    /**
     * Check if field with #fieldName is exists in class or its parent
     * @param tClass class name for searching
     * @param fieldName name of searching field
     * @return if field exists in class or superclass
     */
    private boolean hasField(Class<?> tClass, String fieldName) {
        if (tClass == null) return false;
        List<Field> fields = getColumnAnnotatedFields(true);
        fields.addAll(getHasRelationFields());
        fields.addAll(getBelongsToRelationFields());

        return fields.stream().anyMatch(field -> fieldName.equals(field.getName()));
    }


    /**
     * Get methods with {@link AfterFind} annotation and call each of them
     */
    private void callAfterFindMethods() {
        List<Method> methods = getMethodsByAnnotation(AfterFind.class);
        methods.forEach(method -> {
            method.setAccessible(true);
            try {
                method.invoke(this);
            }
            catch (IllegalAccessException|InvocationTargetException ignore) {}
        });
    }


    /**
     * Get methods with {@link BeforeSave} annotation and call each of them
     */
    void callBeforeSaveMethods() {
        List<Method> methods = getMethodsByAnnotation(BeforeSave.class);

        methods.forEach(method -> {
            method.setAccessible(true);
            try {
                method.invoke(this);
            }
            catch (IllegalAccessException|InvocationTargetException ignore) {}
        });
    }


    /**
     * Check class for {@link Relation} annotated fields
     * {@link Relation#type()} must be {@link Relation.RELATION_TYPE#BELONGS_TO}
     * Add this fields to list and return it
     */
    private List<Field> getBelongsToRelationFields() {
        List<Field> fields = getFieldsByAnnotation(Relation.class);

        return fields.stream().filter(field -> {
            Relation annotation = field.getDeclaredAnnotation(Relation.class);
            return annotation != null && annotation.type().equals(Relation.RELATION_TYPE.BELONGS_TO);
        }).collect(Collectors.toList());
    }


    /**
     * Check class for {@link Relation} annotated fields
     * {@link Relation#type()} must be {@link Relation.RELATION_TYPE#HAS}
     * Add this fields to list and return it
     */
    private List<Field> getHasRelationFields() {
        List<Field> fields = getFieldsByAnnotation(Relation.class);

        return fields.stream().filter(field -> {
            Relation annotation = field.getDeclaredAnnotation(Relation.class);
            return annotation != null && annotation.type().equals(Relation.RELATION_TYPE.HAS);
        }).collect(Collectors.toList());
    }


    /**
     * Find {@link Relation} annotation at field and return {@link Relation#foreignKey()} value
     * @param field in which {@link Relation} annotation should to be found
     * @return {@link Relation#foreignKey()}
     */
    private static String getForeignKeyByRelationField(Field field) {
        Relation annotation = field.getDeclaredAnnotation(Relation.class);
        return !annotation.foreignKey().isEmpty() ? annotation.foreignKey() : null;
    }


    /**
     * Check assigned relation object for {@link Id} annotated field and return its value
     * @param clazz class name of related object
     * @param relationObject current related object
     * @return value of primary key column in related object
     */
    private Long getRelationFieldPrimaryKeyValue(Class<?> clazz, Object relationObject) {
        Field field = getIdAnnotatedField(clazz);
        if (field != null && relationObject != null) {
            field.setAccessible(true);
            try {
                return Long.parseLong(field.get(relationObject).toString());
            }
            catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    /**
     * Check class for fields which has given annotation and return them
     * @param annotationClass class of annotation which should be found
     * @return List of appropriate fields
     */
    private List<Field> getFieldsByAnnotation(Class<? extends Annotation> annotationClass) {
        List<Field> fields = new ArrayList<>();
        getFieldsByAnnotation(fields, annotationClass, this.getClass());

        return fields;
    }

    private static void getFieldsByAnnotation(List<Field> fields, Class<? extends Annotation> annotationClass,
                                              Class<?> clazz) {
        if (clazz == null) return;

        for (Field field : clazz.getDeclaredFields()) {
            if (field.isAnnotationPresent(annotationClass)) {
                fields.add(field);
            }
        }

        getFieldsByAnnotation(fields, annotationClass, clazz.getSuperclass());
    }


    /**
     * Check class for methods which has given annotation and return them
     * @param annotationClass class of annotation which should be found
     * @return List of appropriate methods
     */
    private List<Method> getMethodsByAnnotation(Class<? extends Annotation> annotationClass) {
        List<Method> methods = new ArrayList<>();
        getMethodsByAnnotation(methods, annotationClass, this.getClass());

        return methods;
    }

    private static void getMethodsByAnnotation(List<Method> methods, Class<? extends Annotation> annotationClass,
                                               Class<?> clazz) {
        if (clazz == null) return;

        for (Method method : clazz.getDeclaredMethods()) {
            if (method.isAnnotationPresent(annotationClass))
                methods.add(method);
        }

        getMethodsByAnnotation(methods, annotationClass, clazz.getSuperclass());
    }


    /**
     * Check field in class and superclasses and return it
     * @param fieldName which should be found
     * @return {@link Field}
     */
    private Field getFieldByName(String fieldName) {
        return getFieldByName(this.getClass(), fieldName);
    }

    private static Field getFieldByName(Class<?> clazz, String fieldName) {
        if (clazz == null) return null;

        try {
            return clazz.getDeclaredField(fieldName);
        }
        catch (NoSuchFieldException e) {
            return getFieldByName(clazz.getSuperclass(), fieldName);
        }
    }

    private static Class<?> getFirstGenericType(Field field) {
        ParameterizedType parameterizedType = (ParameterizedType) field.getGenericType();
        return (Class<?>) parameterizedType.getActualTypeArguments()[0];
    }


    private static Class<?> getEndType(Field field) {
        if (Collection.class.isAssignableFrom(field.getType())) {
            return getFirstGenericType(field);
        }

        return field.getType();
    }


    /**
     * Generate new T object and return it
     * @param clazz of new Object
     * @param resultSet collection of data for object
     * @param <T> class of new object
     * @param columnPrefix String refers to column prefix
     * @return new object
     */
    private static <T extends Entity> T fromResultSet(Class<T> clazz, ResultSet resultSet, String columnPrefix) {
        try {
            T result = clazz.newInstance();
            List<Field> fields = new ArrayList<>();

            getColumnAnnotatedFields(fields, clazz, true);

            for (Field field : fields) {
                try {
                    field.setAccessible(true);

                    String columnName = String.format("%s%s_%s", columnPrefix, getTableName(clazz), getColumnNameByField(field));
                    Object value = null;

                    if (field.getType().equals(String.class)) {
                        value = resultSet.getString(columnName);
                    }
                    else if (field.getType().equals(Character.class) || field.getType().equals(char.class)) {
                        value = resultSet.getString(columnName).charAt(0);
                    }
                    else if (field.getType().equals(Double.class) || field.getType().equals(double.class)) {
                        value = resultSet.getDouble(columnName);
                    }
                    else if (field.getType().equals(Float.class) || field.getType().equals(float.class)) {
                        value = resultSet.getFloat(columnName);
                    }
                    else if (field.getType().equals(Long.class) || field.getType().equals(long.class)) {
                        value = resultSet.getLong(columnName);
                    }
                    else if (field.getType().equals(Integer.class) || field.getType().equals(int.class)) {
                        value = resultSet.getInt(columnName);
                    }
                    else if (field.getType().equals(Short.class) || field.getType().equals(short.class)) {
                        value =  resultSet.getShort(columnName);
                    }
                    else if (field.getType().equals(Boolean.class) || field.getType().equals(boolean.class)) {
                        value = resultSet.getBoolean(columnName);
                    }
                    else if (field.getType().equals(LocalDate.class)) {
                        value = resultSet.getObject(columnName, LocalDate.class);
                    }
                    else if (field.getType().equals(LocalTime.class)) {
                        value = resultSet.getObject(columnName, LocalTime.class);
                    }
                    else if (field.getType().equals(LocalDateTime.class)) {
                        value = resultSet.getObject(columnName, LocalDateTime.class);
                    }
                    else if (field.getType().equals(OffsetDateTime.class)) {
                        value = resultSet.getObject(columnName, OffsetDateTime.class);
                    }

                    field.set(result, value);
                }
                catch (SQLException e) {
                    e.printStackTrace();
                }
                catch (IllegalAccessException ignore) {}
                catch (NullPointerException e) {
                    return null;
                }
            }

            Field idAnnotatedField = getIdAnnotatedField(clazz);
            if (idAnnotatedField != null) {
                idAnnotatedField.setAccessible(true);
                try {
                    if (idAnnotatedField.get(result) == null ||
                            Long.parseLong(idAnnotatedField.get(result).toString()) == 0)
                        result = null;
                }
                catch (NumberFormatException e) {
                    result = null;
                }
            }
            else {
                result = null;
            }

            return result;
        }
        catch (InstantiationException|IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    static <T extends Entity> T fromResultSet(Class<T> clazz, ResultSet resultSet) {
        T object = fromResultSet(clazz, resultSet, "");

        List<Field> relationFields = new ArrayList<>();
        getFieldsByAnnotation(relationFields, Relation.class, clazz);

        relationFields.forEach(field -> {
            try {
                field.setAccessible(true);
                Class objectType = getEndType(field);
                Entity currentObject = null;

                if (Entity.class.isAssignableFrom(objectType)) {
                    currentObject = Entity.fromResultSet(objectType, resultSet, String.format("%s_", field.getName()));
                }

                if (currentObject != null) {
                    if (Collection.class.isAssignableFrom(field.getType())) {
                        if (field.get(object) == null) {
                            field.set(object, new ArrayList<>());
                        }
                        ((Collection) field.get(object)).add(currentObject);
                    }
                    else field.set(object, currentObject);
                }
            }
            catch (IllegalAccessException|NullPointerException e) {}
        });

        return object;
    }

    static <T extends Entity> void merge(T objectTo, T objectFrom) {
        if (objectTo.equals(objectFrom)) {
            List<Field> relationFields = new ArrayList<>();
            getFieldsByAnnotation(relationFields, Relation.class, objectTo.getClass());

            relationFields.forEach(field -> {
                try {
                    field.setAccessible(true);

                    if (field.get(objectTo) != null && field.get(objectFrom) != null) {
                        if (Collection.class.isAssignableFrom(field.getType())) {
                            ((Collection) field.get(objectTo)).addAll((Collection) field.get(objectFrom));
                        }
                        else {
                            field.set(objectTo, field.get(objectFrom));
                        }
                    }
                }
                catch (IllegalAccessException|NullPointerException ignore) {}
            });
        }
    }

    public boolean equals(Entity e) {
        String myTableName = getTableName();
        String eTableName = getTableName(e.getClass());

        Field myIdAnnotatedField = getIdAnnotatedField();
        Field eIdAnnotatedField = getIdAnnotatedField(e.getClass());

        try {
            if (myIdAnnotatedField != null && eIdAnnotatedField != null) {
                myIdAnnotatedField.setAccessible(true);
                eIdAnnotatedField.setAccessible(true);

                return myTableName.equals(eTableName) &&
                        myIdAnnotatedField.get(this).equals(eIdAnnotatedField.get(e));
            }
        }
        catch (IllegalAccessException|NullPointerException ignore) {}

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        return Entity.class.isAssignableFrom(obj.getClass())
                ? equals((Entity) obj)
                : super.equals(obj);
    }
}
