package database.base;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

/**
 * This class implements connection to PostgreSQL database
 */
public class Connection implements AutoCloseable {
    private java.sql.Connection connection;
    private PreparedStatement preparedStatement;
    private ResultSet resultSet;

    static private String dbName;
    static private String username;
    static private String password;

    public java.sql.Connection getConnection() throws SQLException {
        prepareConnection();
        return connection;
    }
    private void setConnection(java.sql.Connection connection) {
        this.connection = connection;
    }

    public PreparedStatement getPreparedStatement() {
        return preparedStatement;
    }
    private void setPreparedStatement(PreparedStatement preparedStatement) {
        this.preparedStatement = preparedStatement;
    }


    public ResultSet getResultSet() {
        return resultSet;
    }
    private void setResultSet(ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    public static String getDbName() {
        return dbName;
    }
    public static void setDbName(String dbName) {
        Connection.dbName = dbName;
    }

    public static String getUsername() {
        return username;
    }
    public static void setUsername(String username) {
        Connection.username = username;
    }

    public static String getPassword() {
        return password;
    }
    public static void setPassword(String password) {
        Connection.password = password;
    }

    /**
     * Create new connection to database if not exists or not connected
     * DBName, username and password are takes from postgresql.properties resource file
     * @throws SQLException when SQL exception occurs
     */
    private void prepareConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            setConnection(DriverManager.getConnection(
                    String.format("jdbc:postgresql://localhost:5432/%s", getDbName()),
                    getUsername(), getPassword()));
            disableAutocommit();
        }
    }

    private void disableAutocommit() throws SQLException {
        getConnection().setAutoCommit(false);
    }

    private void loadDBProperties() throws SQLException {
        try (InputStream inputStream = this.getClass().getClassLoader()
                .getResourceAsStream("postgresql.properties")) {
            Properties properties = new Properties();
            properties.load(inputStream);

            if (!properties.containsKey("dbname")) throw new SQLException("Parameter dbname is not set");
            if (!properties.containsKey("username")) throw new SQLException("Parameter username is not set");
            if (!properties.containsKey("password")) throw new SQLException("Parameter password is not set");

            setDbName(properties.getProperty("dbname"));
            setUsername(properties.getProperty("username"));
            setPassword(properties.getProperty("password"));
        }
        catch (IOException e) {
            throw new SQLException("postgresql.properties file not found");
        }
    }

    /**
     * Makes query to database, getting result, applying data to objects and return them
     * @param clazz points to class which need to be queried
     * @param query for database to load data
     * @param <T> is returned object class type
     * @return List<T> list of objects
     * @throws SQLException when SQL exception occurs
     */
    public static <T extends Entity> Collection<T> select(Class<T> clazz, String query) throws SQLException {
        try (Connection connection = new Connection()) {
            connection.execute(query);

            return connection.collectResult(clazz);
        }
    }

    /**
     * Prepare {@link #resultSet} and collect data
     * @param clazz class of queried objects
     * @param <T> type of queried objects
     * @return List of collected items
     * @throws SQLException when SQL exception occurs
     */
    private <T extends Entity> List<T> collectResult(Class<T> clazz) throws SQLException {
        List<T> result = new ArrayList<>();

        while (getResultSet().next()) {
            collectItem(clazz, result);
        }

        return result;
    }

    /**
     * Get data from {@link #resultSet} and add current object to result or merge data with same element in list
     * @param clazz class of queried objects
     * @param result list of existing objects
     * @param <T> type of queried objects
     * @throws SQLException when SQL Exception occurs
     */
    private <T extends Entity> void collectItem(Class<T> clazz, List<T> result) throws SQLException {
        T currentObject = T.fromResultSet(clazz, getResultSet());
        if (currentObject != null) {
            if (result.contains(currentObject)) {
                int index = result.indexOf(currentObject);
                T objectToMerge = result.get(index);
                Entity.merge(objectToMerge, currentObject);
            }
            else result.add(currentObject);
        }
    }

    /**
     * Insert row to the table and return primary column value
     * @param query for inserting new row to database
     * @param idColumnName required for getting generated primary column value from database
     * @return id of new row
     * @throws SQLException when SQL exception occurs
     */
    public static long insert(String query, String idColumnName) throws SQLException {
        try (Connection connection = new Connection()) {
            connection.executeUpdateAndGetGeneratedKeys(query);
            if (connection.getResultSet().next()) {
                return connection.getResultSet().getLong(idColumnName);
            }
            return 0;
        }
    }

    /**
     *
     * @param query for database which should be executed
     * @throws SQLException when SQL exception occurs
     */
    public static void executeSql(String query) throws SQLException {
        try (Connection connection = new Connection()) {
            connection.setPreparedStatement(connection.getConnection().prepareStatement(query));
            connection.executeUpdate(query);
        }
    }

    /**
     * Execute SQL query and set result to {@link #resultSet}
     * @param sql which should be executed
     * @throws SQLException when SQL exception occurs
     */
    public void execute(String sql) throws SQLException {
        setPreparedStatement(getConnection().prepareStatement(sql));
        setResultSet(getPreparedStatement().executeQuery());
    }

    /**
     * Execute SQL update query and set result to {@link #resultSet}
     * @param sql which should be executed
     * @throws SQLException when SQL exception occurs
     */
    public void executeUpdate(String sql) throws SQLException {
        try {
            setPreparedStatement(getConnection().prepareStatement(sql));
            getPreparedStatement().executeUpdate();
            commit();
            setResultSet(getPreparedStatement().getResultSet());
        }
        catch (SQLException e) {
            rollback();
            throw e;
        }
    }

    public void executeUpdateWithoutCommit(String sql) throws SQLException {
        setPreparedStatement(getConnection().prepareStatement(sql));
        getPreparedStatement().executeUpdate();
        setResultSet(getPreparedStatement().getResultSet());
    }

    /**
     * Execute SQL query and set {@link ResultSet} to {@link #resultSet} with generated primary column value
     * @param sql which should be executed
     * @throws SQLException when SQL exception occurs
     */
    public void executeUpdateAndGetGeneratedKeys(String sql) throws SQLException {
        try {
            setPreparedStatement(getConnection().prepareStatement(sql));
            getPreparedStatement().executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            commit();
            setResultSet(getPreparedStatement().getGeneratedKeys());
        }
        catch (SQLException e) {
            rollback();
            throw e;
        }
    }

    public void commit() throws SQLException {
        getConnection().commit();
    }

    public void rollback() throws SQLException {
        getConnection().rollback();
    }


    /**
     * Loads PostgreSQL driver and creates connection
     * @throws SQLException when SQL exception occurs
     */
    public Connection() throws SQLException {
        loadDBProperties();
        prepareConnection();
    }

    @Override
    public void close() throws SQLException {
        if (getResultSet() != null && !getResultSet().isClosed()) getResultSet().close();
        if (getPreparedStatement() != null && !getPreparedStatement().isClosed()) getPreparedStatement().close();
        if (getConnection() != null && !getConnection().isClosed()) getConnection().close();
    }
}