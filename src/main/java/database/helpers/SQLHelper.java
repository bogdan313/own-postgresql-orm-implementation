package database.helpers;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

public class SQLHelper {
    public static String getSQLString(Object object) {
        if (object == null) {
            return "null";
        }

        if (object.getClass().equals(String.class) ||
                object.getClass().equals(Character.class) || object.getClass().equals(char.class) ||
                object.getClass().equals(LocalDate.class) || object.getClass().equals(LocalTime.class) ||
                object.getClass().equals(LocalDateTime.class) || object.getClass().equals(OffsetDateTime.class))
            return String.format("'%s'", escapeSingleQuotesFromString(object.toString()));

        if (object instanceof List) {
            return String.format("(%s)",
                    ((List) object).stream().map(SQLHelper::getSQLString).collect(Collectors.joining(", ")));
        }

        return object.toString();
    }

    public static String getFieldValue(Field field, Object object) {
        try {
            field.setAccessible(true);
            return getSQLString(field.get(object));
        }
        catch (IllegalAccessException|NullPointerException e) {}

        return null;
    }

    public static String escapeSingleQuotesFromString(String string) {
        return string.replaceAll("\'", "");
    }

    public static String escapeDoubleQuotesFromString(String string) {
        return string.replaceAll("\"", "");
    }
}
